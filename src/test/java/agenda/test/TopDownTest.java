package agenda.test;

import agenda.exceptions.InvalidFormatException;
import agenda.model.base.Activity;
import agenda.model.base.Contact;
import agenda.model.repository.classes.RepositoryActivityMock;
import agenda.model.repository.classes.RepositoryContactMock;
import agenda.model.repository.interfaces.RepositoryActivity;
import agenda.model.repository.interfaces.RepositoryContact;
import org.junit.Before;
import org.junit.Test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class TopDownTest {

    private Contact contact;
    private RepositoryActivity repAct;
    private RepositoryContact repCon;

    @Before
    public void setup() throws Exception {
        repCon = new RepositoryContactMock();
        repAct = new RepositoryActivityMock();

        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        Activity act1 = new Activity("activity1", df.parse("04/30/2018 12:00"),
                df.parse("04/30/2018 20:00"),
                null,
                "Meal break");
        repAct.addActivity(act1);
//			activities.add(act);
        Activity act2 = new Activity("activity2", df.parse("04/30/2018 20:00"),
                df.parse("04/30/2013 21:00"),
                null,
                "Meal break");
        repAct.addActivity(act2);
//        for (Activity a : repAct.getActivities())
//            repAct.removeActivity(a);
    }

    @Test
    public void testCaseAddValidAddContact() {
        try {
            contact = new Contact("name", "address1", "+4071122334455", "test1@email.com");
        } catch (InvalidFormatException e) {
            assertTrue(false);
        }
        //int n = rep.count();
        repCon.addContact(contact);
        for (Contact c : repCon.getContacts())
            if (c.equals(contact)) {
                assertTrue(true);
                break;
            }
    }

    @Test
    public void integrareAB() {
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        List<Contact> contacts = new ArrayList<Contact>();
        contacts.add(contact);
        int size = repAct.getActivities().size();
        try {
            Activity act = new Activity("integrare",
                    df.parse("05/04/2018 14:15"),
                    df.parse("05/04/2018 21:30"),
                    contacts,
                    "Lunch break");


            assertTrue(repAct.addActivity(act));
        } catch (Exception e) {
        }
        assertTrue(size + 1 == repAct.countActivities());
        for (Activity act : repAct.getActivities())
            if (act.getContacts().contains(contact)) {
                assertTrue(true);
                break;
            }
    }



    @Test
    public void integrareABC() {
        integrareAB();
        Calendar c = Calendar.getInstance();
        c.set(2018, Calendar.MAY, 4);
        List<Activity> result = repAct.activitiesOnDate("integrare", c.getTime());

        assertTrue(result.size() == 1);
    }

}
